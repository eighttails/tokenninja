import React from 'react';
import ReactDOM from 'react-dom';
import { IntlProvider } from 'react-intl';

import App from './views/App';
import registerServiceWorker from './registerServiceWorker';

// TODO Remove this, and put CSS in components
import './assets/css/semantic/semantic.min.css';
import './assets/css/main.css';

ReactDOM.render(
  <IntlProvider locale="en">
    <App />
  </IntlProvider>,
  document.getElementById('root')
);
registerServiceWorker();
