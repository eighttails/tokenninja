/**
 * Trim a string that is too long (like public address) to show 8 first chars
 * and 4 last chars
 * @param {String} string
 */
const trimUsername = string =>
  string.length > 14
    ? `${string.substring(0, 8)}...${string.substr(string.length - 4)}`
    : string;

export default trimUsername;
