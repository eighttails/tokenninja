import React, { Component } from 'react';

import Header from '../Header';
import Portfolio from '../Portfolio';
import './App.css';

class App extends Component {
  render() {
    return [
      <Header key="Header" />,
      <div className="main-wrapper" key="App-main-wrapper">
        <div id="token-main">
          <Portfolio />
        </div>
      </div>
    ];
  }
}

export default App;
