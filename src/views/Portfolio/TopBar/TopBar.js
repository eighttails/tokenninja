import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Menu, Segment } from 'semantic-ui-react';

import { TABS } from '../Portfolio';
import './TopBar.css';

class TopBar extends Component {
  static propTypes = {
    currentTab: PropTypes.string.isRequired,
    onChangeTab: PropTypes.func.isRequired
  };

  handleAllowClick = () => this.props.onChangeTab(TABS.ALLOW);

  handleReceiveClick = () => this.props.onChangeTab(TABS.RECEIVE);

  handleSendClick = () => this.props.onChangeTab(TABS.SEND);

  render() {
    const { currentTab } = this.props;
    return (
      <Segment attached="top" className="header-segment" raised>
        <h3>
          <FormattedMessage
            defaultMessage="TokenNinja"
            description="Portfolio title"
            id="Portfolio.Topbar.title"
          />
        </h3>
        <Menu pointing secondary>
          <Menu.Item
            active={currentTab === TABS.SEND}
            onClick={this.handleSendClick}
          >
            <FormattedMessage
              defaultMessage="Send"
              description="Portfolio send tab"
              id="Portfolio.Topbar.sendTab"
            />
          </Menu.Item>
          <Menu.Item
            active={currentTab === TABS.RECEIVE}
            onClick={this.handleReceiveClick}
          >
            <FormattedMessage
              defaultMessage="Receive"
              description="Portfolio receive tab"
              id="Portfolio.Topbar.receiveTab"
            />
          </Menu.Item>
          <Menu.Item
            active={currentTab === TABS.ALLOW}
            onClick={this.handleAllowClick}
          >
            <FormattedMessage
              defaultMessage="Allowance"
              description="Portfolio allowance tab"
              id="Portfolio.Topbar.allowTab"
            />
          </Menu.Item>
        </Menu>
      </Segment>
    );
  }
}

export default TopBar;
