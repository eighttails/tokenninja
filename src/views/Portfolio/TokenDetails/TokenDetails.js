import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Search } from 'semantic-ui-react';

import SendToken from './SendToken';
import './TokenDetails.css';

class TokenDetails extends Component {
  static propTypes = {
    token: PropTypes.object
  };

  render() {
    const { token } = this.props;
    if (!token) return null;
    return (
      <div className="ui tab active">
        <div className="token-details__flex">
          <div className="token-details__top">
            <h3 className="token-details__name">
              <a
                className="theme-color"
                href={`https://etherscan.io/address/${token.address}`}
                target="_blank"
              >
                {token.name}
              </a>
            </h3>
            <p className="token-details__description">{token.description}</p>
          </div>
          <div className="token-details__form">
            <div className="token-details__balance">
              <div className="key">
                <FormattedMessage
                  defaultMessage="Balance"
                  description="Token balance label"
                  id="Portfolio.TokenDetails.balance"
                />
              </div>
              <div className="value">18 {token.shortName}</div>
            </div>
            <SendToken />
          </div>
        </div>
      </div>
    );
  }
}

export default TokenDetails;
