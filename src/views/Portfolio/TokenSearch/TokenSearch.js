import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { injectIntl, intlShape } from 'react-intl';
import { Input } from 'semantic-ui-react';

import './TokenSearch.css';

@injectIntl
class TokenSearch extends Component {
  static propTypes = {
    intl: intlShape,
    onChange: PropTypes.func.isRequired
  };

  handleChange = (_, { value }) => this.props.onChange(value);

  render() {
    const { intl: { formatMessage } } = this.props;
    return (
      <Input
        fluid
        icon="search"
        onChange={this.handleChange}
        placeholder={formatMessage({
          defaultMessage: 'Search tokens',
          description: 'Search tokens placeholder',
          id: 'Portfolio.TokenSearch.placeholder'
        })}
      />
    );
  }
}

export default TokenSearch;
