import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { Responsive, Segment } from 'semantic-ui-react';

import ethLogo from '../../assets/img/token/eth-logo.svg';
import TokenDetails from './TokenDetails';
import TokenList from './TokenList';
import TokenSearch from './TokenSearch';
import TopBar from './TopBar';
import './Portfolio.css';

export const TABS = {
  ALLOW: 'ALLOW_TAB',
  RECEIVE: 'RECEIVE_TAB',
  SEND: 'SEND_TAB'
};

class Portfolio extends PureComponent {
  state = {
    currentTab: TABS.SEND,
    filter: null,
    token: null
  };

  handleChangeFilter = filter => this.setState({ filter });

  handleChangeTab = currentTab => this.setState({ currentTab });

  handleChangeToken = token => {
    this.setState({ token });
  };

  render() {
    return (
      <div className="content-wrapper show-list">
        <Responsive className="token-background" minWidth={1365}>
          <img alt="eth-logo" src={ethLogo} />
        </Responsive>
        <TopBar
          currentTab={this.state.currentTab}
          onChangeTab={this.handleChangeTab}
        />
        <Segment attached="bottom" className="main-segment" raised>
          <div className="token-container">
            <div className="token-selector">
              <TokenSearch onChange={this.handleChangeFilter} />
              <TokenList
                filter={this.state.filter}
                onTokenSelect={this.handleChangeToken}
              />
            </div>
            <TokenDetails token={this.state.token} />
          </div>
        </Segment>
        <p className="token-ninja-description">
          <FormattedMessage
            defaultMessage="TokenNinja is an ERC20 Token Dapp"
            description="Token ninja description"
            id="Portfolio.description"
          />
        </p>
      </div>
    );
  }
}

export default Portfolio;
