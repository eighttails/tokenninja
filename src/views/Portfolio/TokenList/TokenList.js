import React, { Component } from 'react';
import PropTypes from 'prop-types';

import tokens from '../../../assets/tokens.json';
import trimUsername from '../../../utils/trimUsername';
import './TokenList.css';

class TokenList extends Component {
  static propTypes = {
    filter: PropTypes.string,
    onTokenSelect: PropTypes.func.isRequired
  };

  handleClick = token => this.props.onTokenSelect(token);

  render() {
    const { filter } = this.props;
    return (
      <div className="token-list">
        {tokens
          .filter(token => {
            if (!filter) return true;

            return (
              token.name.toLowerCase().includes(filter.toLowerCase()) ||
              token.shortName.toLowerCase().includes(filter.toLowerCase())
            );
          })
          .map(token => (
            <div
              className="token-list__item"
              key={token.shortName}
              onClick={() => this.handleClick(token)} // TODO Do not use new function ref
            >
              <div className="token-list__icon">
                <img src={token.icon} alt={token.shortName} />
              </div>
              <div className="token-list__info">
                <h4 className="token-list__name">{token.shortName}</h4>
                <div className="token-list__address">
                  {trimUsername(token.address)}
                </div>
              </div>
            </div>
          ))}
      </div>
    );
  }
}

export default TokenList;
