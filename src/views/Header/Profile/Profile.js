import React, { Component } from 'react';

import injectWeb3 from '../../../utils/injectWeb3';
import './Profile.css';

@injectWeb3()
class Profile extends Component {
  state = {
    publicAddress: null
  };

  async componentWillMount() {
    const { web3 } = this.props;
    const publicAddress = await web3.eth.getCoinbase();
    this.setState({ publicAddress });
  }

  render() {
    return (
      <div className="profile-icon user-icon">{this.state.publicAddress}</div>
    );
  }
}

export default Profile;
