import React from 'react';
import { FormattedMessage } from 'react-intl';

import './Navigation.css';

const Navigation = () => (
  <nav>
    <a href="https://beta.p2pworld.io">
      <FormattedMessage
        defaultMessage="Back to Main Site"
        description="Back to main site button"
        id="Header.Navigation.backToMainSiteButton"
      />
    </a>
  </nav>
);

export default Navigation;
