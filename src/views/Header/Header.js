import React from 'react';

import Navigation from './Navigation';
import Profile from './Profile';
import './Header.css';

const Header = () => (
  <header className="site-header site-header--tokenninja">
    <div className="header-wrapper">
      <a className="site-name" href="https://beta.p2pworld.io">
        P2PWorld
      </a>{' '}
      /{' '}
      <a className="site-product" href="/">
        TokenNinja
      </a>
      <Navigation />
      <Profile />
    </div>
  </header>
);

export default Header;
