const { compose } = require('react-app-rewired');
const rewireMobx = require('react-app-rewire-mobx');

module.exports = function(config, env) {
  const rewires = compose(rewireMobx);
  // do custom config
  // ...
  return rewires(config, env);
};
